const  {Router} = require('express');
const router = Router();
const {renderAddItemPortfolio,
    renderDeleteItemPortfolio,
    renderEditItemPortfolio,
    renderItemPortfolio,
    renderPortfolio,renderPostAddItemPortfolio,renderPutPOSTEditItemPortfolio,renderPutGETEditItemPortfolio} = require('../controllers/bien.controller');

const {isAuthenticated} =require('../helpers/auth');


router.get('/portfolio',renderPortfolio);

router.get('/portfolio/item/:id',renderItemPortfolio);

router.get('/portfolio/add',isAuthenticated,renderAddItemPortfolio);
router.post('/portfolio/new-bien',isAuthenticated,renderPostAddItemPortfolio);

router.get('/portfolio/edit',isAuthenticated,renderEditItemPortfolio);
router.get('/portfolio/edit/:id',isAuthenticated,renderPutGETEditItemPortfolio);
router.put('/portfolio/postedit/:id',isAuthenticated,renderPutPOSTEditItemPortfolio);

router.delete('/portfolio/delete/:id',isAuthenticated,renderDeleteItemPortfolio);


module.exports = router;