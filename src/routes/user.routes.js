const {Router} = require('express');
const router = Router();
const {renderSingIn,renderSingInForm,renderLogOut,renderSingUp,renderSingUpForm} =require('../controllers/user.controller');
const {isAuthenticated} =require('../helpers/auth');


router.get('/users/singup',renderSingUpForm);
router.post('/users/singup',renderSingUp);
router.get('/users/singin',renderSingInForm);
router.post('/users/singin',renderSingIn);
router.get('/users/logout',renderLogOut);



module.exports = router;