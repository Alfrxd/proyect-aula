const usersCrtl= {};
const User = require('../models/user');
const passport = require('passport');


usersCrtl.renderSingUpForm= (req,res) =>{
    res.render('register_user');
};
usersCrtl.renderSingUp= async (req,res) =>{
    const erros = [];    
    const {repeatidinmueble,idinmueble,repeatpassword,password,cedula} = req.body;
    if(password != repeatpassword){
        erros.push({text:'Las Contrasenas no coinciden'});
    }
    if(idinmueble != repeatidinmueble){
        erros.push({text:'Los ids de Inmueble no coinciden'});
    }
    if(password.length < 4){
        erros.push({text:'La Contrasena es muy corta'});
    }
    if(erros.length > 0){
        res.render('register_user',{
            erros
        });
    }
    else{
        const newUser = new User({id_User:cedula,password:password,id_Inmueble:idinmueble});
        newUser.password = await newUser.encrypPassword(password);
        await newUser.save();
        
        res.render('admin');
        


    }

    
};
usersCrtl.renderSingInForm= (req,res) =>{
    res.render('login');
};
usersCrtl.renderSingIn= passport.authenticate('local',{
    failureRedirect: '/login',
    successRedirect: '/admin'
})
usersCrtl.renderLogOut= (req,res) =>{
    req.logout();
    res.redirect('/');
};



module.exports = usersCrtl;