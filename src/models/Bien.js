const { Mongoose } = require("mongoose");

const {Schema, model} = require('mongoose');

const BienSchema = new Schema({
    Titulo: String,
    Descripcion: String,
    Direccion: String,
    Habitaciones: String,
    Patio: String,
    Cielo_Razo: String,
    Ceramica: String,
    Cocina: String,
    Banos: String,
    Garage: String,
    Amoblada: String,
    Estrato: String,
    Tipo_Planta: String,
    Tipo_Medidor_Electrico: String

});
module.exports = model('Bien',BienSchema);
