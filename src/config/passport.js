const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/user'); 

passport.use(new LocalStrategy({
  usernameField: 'cedula'
}, async (cedula, password, done) => {
  // Match Email's User
  const user = await User.findOne({id_User: cedula});
  if (!user || cedula !='1067961864') {
    return done(null, false, { message: 'No Existe El Usuario.' });
  } else {
    

    
      // Match Password's User
    const match = await user.matchPassword(password);
    if(match) {
      return done(null, user);
    } else {
      return done(null, false, { message: 'Incorrect Password.' });
    }  
    
  }
}));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});