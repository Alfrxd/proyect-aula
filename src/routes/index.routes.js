const  {Router} = require('express');
const router = Router();
const {renderAdmin,renderIndex,renderNotificar,rendermodifContrat,renderAbout,renderMessage,renderContact,renderImprDoc,renderLogin} = require('../controllers/index.controller');
const {isAuthenticated} =require('../helpers/auth');

router.get('/',renderIndex);
router.get('/about',renderAbout);
router.get('/contact',renderContact);
router.get('/imprDoc',renderImprDoc);
router.get('/message',renderMessage);
router.get('/login',renderLogin);
router.get('/admin',isAuthenticated,renderAdmin);
router.get('/admin/notificar',isAuthenticated,renderNotificar);
router.get('/admin/modificarcontrato',isAuthenticated,rendermodifContrat);
module.exports = router;