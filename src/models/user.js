const { Mongoose } = require("mongoose");
const {Schema, model} = require('mongoose');
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
    id_User:{type: Number,required: true},
    password:{type:String,required:true},
    id_Inmueble:{type:String,required:true}

});
UserSchema.methods.encrypPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
};
UserSchema.methods.matchPassword = async function(password){
    return await bcrypt.compare(password,this.password);
};


module.exports = model('User',UserSchema);