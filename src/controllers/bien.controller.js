const bienCtrl = {};
const { Schema } = require('mongoose');
const Bien = require('../models/Bien');


bienCtrl.renderItemPortfolio= async(req,res)=>{
    const bien =await Bien.findById(req.params.id);
    res.render('itemPortfolio',{bien});
};
bienCtrl.renderAddItemPortfolio=(req,res)=>{
    
    res.render('new-bien')
};
bienCtrl.renderPostAddItemPortfolio= async (req,res)=>{
    
    const {Titulo,Descripcion,Direccion,
    Habitaciones,       
    Patio,
    Cielo_Razo,
    Ceramica,
    Cocinas,
    Banos,
    Garage,
    Amoblada,
    Estrato,
    Tipo_Planta,
    Tipo_Medidor_Electrico}=(req.body);

    const bien = new  Bien({Titulo,Descripcion,Direccion,
        Habitaciones,       
        Patio,
        Cielo_Razo,
        Ceramica,
        Cocinas,
        Banos,
        Garage,
        Amoblada,
        Estrato,
        Tipo_Planta,
        Tipo_Medidor_Electrico});
        await bien.save();
        
    res.redirect('/admin')
};
bienCtrl.renderPortfolio= async (req,res)=>{
    const bien = await Bien.find();    
    res.render('portfolio', {bien});
};

bienCtrl.renderEditItemPortfolio=async (req,res)=>{
    const bien = await Bien.find();
    res.render('modif',  {bien});
};
bienCtrl.renderPutGETEditItemPortfolio=async (req,res)=>{
    const bien = await Bien.findById(req.params.id);
    res.render('modificar',  {bien});
};
bienCtrl.renderPutPOSTEditItemPortfolio=async (req,res)=>{
    const{
        Titulo,
        Descripcion,
        Direccion,
        Habitaciones,
        Patio,
        Cielo_Razo,
        Ceramica,
        Cocina,
        Banos,
        Garage,
        Amoblada,
        Estrato,
        Tipo_Planta,
        Tipo_Medidor_Electrico
    } = req.body;
    await Bien.findByIdAndUpdate(req.params.id,{Titulo,
        Descripcion,
        Direccion,
        Habitaciones,
        Patio,
        Cielo_Razo,
        Ceramica,
        Cocina,
        Banos,
        Garage,
        Amoblada,
        Estrato,
        Tipo_Planta,
        Tipo_Medidor_Electrico});
    res.redirect('/portfolio/edit');
};
bienCtrl.renderDeleteItemPortfolio= async(req,res)=>{
    await Bien.findByIdAndDelete(req.params.id);
    res.redirect('/portfolio/edit')
};





module.exports = bienCtrl;